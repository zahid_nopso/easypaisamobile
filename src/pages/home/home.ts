import { Component } from '@angular/core';
import { NavController, ToastController, AlertController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import * as papa from 'papaparse';
import { File } from '@ionic-native/file';
import { EmailComposer } from '@ionic-native/email-composer';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public billHeader = 'UTILITY,COMPANY,CONSUMER NO,MOBILE NUMBER'
  public mepcoPrefix = 'Electricity,MEPCO,'
  public sngplPrefix = 'Gas,SNGPL,'

  num: string;
  billType: string
  public refencesNumberArray = []
  public allRefsString = ''
  public fields = []
  public isBillTypeMepco = true
  public isToastAlreadyShowing = false

  constructor(public navCtrl: NavController,
    private barcodeScanner: BarcodeScanner,
    private toastCtrl: ToastController,
    private file: File,
    private emailComposer: EmailComposer,
    private alertCtrl: AlertController,
  ) {
    this.billType = 'mepco'
  }

  ionViewDidLoad() {

  }
  // new scan method
  scan() {
    this.barcodeScanner.scan().then(data => {
      // this is called when a barcode is found
      this.num = data.text
      if (this.num.trim().length > 0) {
        this.isBillTypeMepco ? this.num = this.num.slice(1, 15) : this.num = this.num.slice(5, 14)
        if (this.refencesNumberArray.indexOf(this.num) === -1) {
          

          this.refencesNumberArray.push(this.num)

        } else {

          let abc = ''
          abc.slice(2)
          this.presentToast('Already exists.')
        }

      }


    });
  }
  btnMepcoTapped() {

    if (this.refencesNumberArray && this.refencesNumberArray.length > 0 && !this.isBillTypeMepco) {
      this.presentConfirmationDialog('This will remove all the SNGPL bills you have added?')
    } else {
      this.isBillTypeMepco = true
      this.billType = 'mepco'
    }

  }
  btnSngplTapped() {
    if (this.refencesNumberArray && this.refencesNumberArray.length > 0 && this.isBillTypeMepco) {
      this.presentConfirmationDialog('This will remove all the MEPCO bills you have added?')
    } else {
      this.isBillTypeMepco = false
      this.billType = 'sngpl'
    }



  }

  presentToast(msg) {
    if (this.isToastAlreadyShowing) {
      return 
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
      cssClass: 'rtToastClass'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      this.isToastAlreadyShowing = false
    });

    toast.present().then(jobsDone => {
      this.isToastAlreadyShowing = true
    })
  }

  testEmail() {
    if (!this.refencesNumberArray || this.refencesNumberArray.length === 0) {
      this.presentToast('No bill added yet.')
      return
    }
    let fileName = new Date()
    let _fileName = fileName.getDate() + ' ' + fileName.getMonth() + ' ' + fileName.getFullYear() + ' ' + this.refencesNumberArray.length + '.txt'
    
    this.refencesNumberArray.forEach(ref => {
      if (this.isBillTypeMepco) {
        this.allRefsString += this.mepcoPrefix + ref + ',' + '03366300996' + '\n'
      } else {
        this.allRefsString += this.sngplPrefix + ref + ',' + '03366300996' + '\n'
      }


    });
    this.allRefsString = this.billHeader + '\n' + this.allRefsString
    console.log(this.allRefsString);

    this.file.writeFile(this.file.dataDirectory, _fileName, this.allRefsString, { replace: true })
      .then(() => {

        let email = {
          to: 'zahid@nopso.co.uk',

          attachments: [
            this.file.dataDirectory + _fileName
          ],

          subject: 'Reference no extracted from barcode ' + _fileName,
          body: 'Download attachment',
          isHtml: true
        };
        this.emailComposer.open(email).then(ready => {

        })

      })
      .catch((err) => {
        console.error(err);
      });

  }

  presentConfirmationDialog(msg: string) {
    const alert = this.alertCtrl.create({
      title: 'Are you sure',
      message: msg,
      buttons: [
        {
          text: 'Yes',
          role: 'Yes',
          handler: () => {
            if (!this.isBillTypeMepco) {
              this.isBillTypeMepco = true
              this.billType = 'mepco'
            } else {
              this.isBillTypeMepco = false
              this.billType = 'sngpl'
            }
            this.refencesNumberArray = []
            console.log('Yes clicked');
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('you canceled this action...');
          }
        }
      ]
    });
    alert.present();
  }
}
